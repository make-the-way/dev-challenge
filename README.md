# Make the Way
```
          .--.
::\`--._,'.::.`._.--'/::::
::::.  ` __::__ '  .::::::
::::::-:.`'..`'.:-::::::::
::::::::\ `--' /::::::::::

```

TODO - Pequeno texto de apresentação da empresa


## Desafio
Construir uma aplicação web, utilizando a linguagem e frameworks de sua preferência, que seja capaz de:
- Fazer upload do [arquivo](example/ACME_CORPORATION.csv);
- Ler o arquivo e armazenar o conteúdo em um banco de dados;
- Exibir uma página listando os dados(ex: empresa, notas fiscais e fornecedor);
- Expor o conteúdo através de uma api(RESTful).

Para mais detalhes, [ver documentação](docs/README.md)

## Requisitos:
- O código no repositório público do GitHub, Gitlab ou Bitbucket;
- Utilizar Postgres, MySQL ou o banco de dados de sua preferência ;
- Testes automatizados;
- Bom design de código, respeitando padrões e boas práticas como: SOLID, Clean Code, GoF, etc...
- Inglês técnico;
* *Fazer uso de docker é um diferencial!!*

Ao finalizar, faça um Pull Request neste repositório e avise-nos por email.

O candidato terá uma semana para construir a aplicação.
